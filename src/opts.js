'use strict'

const opts = {
  workdir: '/tmp/package-hunter-workdir',
  jobStore: {},
  pendingContainer: {}
}

module.exports = opts
