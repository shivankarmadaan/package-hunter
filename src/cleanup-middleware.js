'use strict'

const debug = require('debug')('pkgs:cleanup-middleware')
const fs = require('fs').promises

module.exports = async function (req, res, next) {
  res.on('close', async function () {
    // delete archive
    if (req.tmpPath) {
      debug(`removing ${req.tmpPath}`)
      try {
        await fs.unlink(req.tmpPath)
      } catch (err) {
        debug(err)
      }
    } else {
      debug('nothing to remove for %s', req.tmpPath)
    }

    // delete files/folders that have been unpacked
    if (req.extractDestinationPath) {
      debug(`removing ${req.extractDestinationPath}`)
      try {
        await fs.rmdir(req.extractDestinationPath, { recursive: true })
      } catch (err) {
        debug(err)
      }
    } else {
      debug('nothing to remove for %s', req.extractDestinationPath)
    }
  })
  next()
}
