'use strict'

const conf = require('./config')

function initSandboxManager (app) {
  if (conf.sandboxExecutor !== 'docker' && conf.sandboxExecutor !== 'k8s') {
    throw new Error(`Invalid conf.sandboxExecutor: ${conf.sandboxExecutor}`)
  }

  const sandboxManager = {}

  if (conf.sandboxExecutor === 'docker') {
    sandboxManager.setupEventListener = require('./falco')

    sandboxManager.middlewareFactory = require('./docker-middleware-factory')
  } else { // k8s
    const listener = require('./sidekickevents-middleware')
    sandboxManager.setupEventListener = () => listener(app)

    sandboxManager.middlewareFactory = require('./k8s-middleware-factory')
  }

  return sandboxManager
}

module.exports = initSandboxManager
