const debug = require('debug')('pkgs:route')

const opts = require('./opts.js')

module.exports = function (req, res, next) {
  debug('GET /')
  const id = req.query && req.query.id
  if (!id) {
    res.statusCode = 400
    res.json({ status: 'error', reason: 'required parameter \'id\' is missing' })
    return
  }

  if (!(id in opts.jobStore)) {
    debug(`no job with ${id} found`)
    res.json({ status: 'error', reason: `unknown id ${id}` })
    return
  }

  res.json(opts.jobStore[id])

  if (opts.jobStore[id].status !== 'finished') { return }

  // Cleanup job if finished
  delete opts.jobStore[id]
  for (const container in opts.pendingContainer) {
    if (opts.pendingContainer[container] === id) {
      delete opts.pendingContainer[container]
      break
    }
  }

  debug(`Job ${id} is cleaned up.`)
  next()
}
