'use strict' /* eslint-env mocha */

// const app = require('express')()
const createAuthMiddleware = require('../src/auth-middleware')
const sinon = require('sinon')
const assert = require('assert')

describe('Auth Middleware Test', function () {
  before(function () {
    this.originalNodeEnv = process.env.NODE_ENV

    this.stubResponse = function () {
      const res = {}
      res.status = (...args) => res
      res.json = () => res
      res.send = () => res
      return res
    }
  })

  after(function () {
    if (this.originalNodeEnv) { process.env.NODE_ENV = this.originalNodeEnv }
  })

  beforeEach(function () {
    this.emptyStore = []
    this.storeWithTwoUsers = [
      { name: 'testuser', hash: '$2b$14$GGj1Va04wpt/B3NhAowGZeWyuItPqytRh/VEFGYI.MBQbi8hRDrNO' }, // password is "password"
      { name: 'anotheruser', hash: '$2b$14$n9b9CpezynM6eLiumCOxn.vTrhrNE9qpM.WFnA90Ue9xmBEua05Ry' } // password is "foo"
    ]
  })

  it('skips auth if NODE_ENV is development and no users configured', function () {
    process.env.NODE_ENV = 'development'

    const auth = createAuthMiddleware(this.emptyStore)
    const [req, res, nextSpy] = [null, null, sinon.spy()]
    auth(req, res, nextSpy)
    assert(nextSpy.calledOnce)
  })

  it('does not skip auth if NODE_ENV is development and users are configured', function () {
    process.env.NODE_ENV = 'development'

    const auth = createAuthMiddleware(this.storeWithTwoUsers)
    const unauthenticatedReq = { headers: {} } // Authorization header is missing, req send without credentials
    const nextSpy = sinon.spy()
    auth(unauthenticatedReq, this.stubResponse(), nextSpy)
    assert(nextSpy.notCalled)
  })

  it('does not skip auth if NODE_ENV is not development', function () {
    process.env.NODE_ENV = 'not_development'

    const auth = createAuthMiddleware(this.emptyStore)
    const req = { headers: {} }
    const nextSpy = sinon.spy()
    auth(req, this.stubResponse(), nextSpy)
    assert(nextSpy.notCalled)
  })
})
