'use strict' /* eslint-env mocha */

const authorizer = require('../src/authorizer')
const sinon = require('sinon')

describe('Authorizer Test', function () {
  beforeEach(function () {
    this.alice = { name: 'alice', password: 'secret', hash: 'abcd' }
    this.store = [this.alice]

    this.comparestub = sinon.stub()
    this.hashfunc = { compare: this.comparestub }
    this.authorizer = authorizer(this.store, this.hashfunc)
  })

  it('passes true to callback if username and password exist', async function () {
    this.comparestub.returns(true)
    const cb = sinon.spy()
    await this.authorizer(this.alice.name, this.alice.password, cb)
    sinon.assert.calledWith(cb, null, true)
  })

  it('passes false to callback if username does not exist', async function () {
    this.comparestub.returns(true)
    const cb = sinon.spy()
    await this.authorizer('name_does_not_exist', this.alice.password, cb)
    sinon.assert.calledWith(cb, null, false)
  })

  it('passes false to callback if password does not exist', async function () {
    this.comparestub.returns(false)
    const cb = sinon.spy()
    const wrongpassword = 'pass_does_not_exist'
    await this.authorizer(this.alice.name, wrongpassword, cb)
    sinon.assert.calledWith(cb, null, false)
    sinon.assert.calledWith(this.comparestub, wrongpassword)
  })
})
